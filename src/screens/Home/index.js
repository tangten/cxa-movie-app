import React from "react";
import { connect } from "react-redux";
import { ScrollView } from "react-native";
import { NavigationBar, Title, Screen } from "@shoutem/ui";
import {
  getTopMovies,
  getPopularMovies,
  getUpcomingMovies
} from "~/redux/actions/movie";
import {
  getTopMovies as topMovies,
  getPopularMovies as popularMovies,
  getUpcomingMovies as upcomingMovies
} from "~/redux/selectors";

import MovieSection from "./MovieSection";

class HomeScreen extends React.Component {
  render() {
    const {
      isLoadingTopMovies,
      getTopMovies,
      topMovies,
      isLoadingPopularMovies,
      getPopularMovies,
      popularMovies,
      isLoadingUpcomingMovies,
      upcomingMovies,
      getUpcomingMovies
    } = this.props;

    return (
      <Screen>
        <NavigationBar
          centerComponent={<Title>CXA MOVIES</Title>}
          styleName="inline"
        />
        <ScrollView>
          <MovieSection
            isLoading={isLoadingTopMovies}
            title="Top Rated Movies"
            movies={topMovies}
            getMovies={getTopMovies}
            navigation={this.props.navigation}
          />
          <MovieSection
            isLoading={isLoadingPopularMovies}
            title="Popular Movies"
            movies={popularMovies}
            getMovies={getPopularMovies}
            navigation={this.props.navigation}
          />
          <MovieSection
            isLoading={isLoadingUpcomingMovies}
            title="Upcoming Movies"
            movies={upcomingMovies}
            getMovies={getUpcomingMovies}
            navigation={this.props.navigation}
          />
        </ScrollView>
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoadingTopMovies: state.movie.isLoadingTopMovies,
    topMovies: topMovies(state),
    isLoadingPopularMovies: state.movie.isLoadingPopularMovies,
    popularMovies: popularMovies(state),
    isLoadingUpcomingMovies: state.movie.isLoadingUpcomingMovies,
    upcomingMovies: upcomingMovies(state)
  };
};

export default connect(
  mapStateToProps,
  { getTopMovies, getPopularMovies, getUpcomingMovies }
)(HomeScreen);
