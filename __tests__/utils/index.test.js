import Config from "react-native-config";
import { getMoviePoster, getMovieBackdrop, buildUrl } from "~/utils";
import {
  DEFAULT_MOVIE_POSTER_URL,
  DEFAULT_MOVIE_BACKDROP_URL
} from "~/constants";

describe("Utils", () => {
  describe("getMoviePoster function", () => {
    test("get movie poster url success with valid path", () => {
      const path = getMoviePoster("test.png");
      expect(path).toEqual(`${Config.THE_MOVIE_DB_POSTER_PATH}/test.png`);
    });

    test("get default movie poster url with empty path", () => {
      const path = getMoviePoster();
      expect(path).toEqual(DEFAULT_MOVIE_POSTER_URL);
    });
  });

  describe("getMovieBackdrop function", () => {
    test("get movie backdrop url success with valid path", () => {
      const path = getMovieBackdrop("test.png");
      expect(path).toEqual(`${Config.THE_MOVIE_DB_POSTER_PATH}/test.png`);
    });

    test("get default movie backdrop url with empty path", () => {
      const path = getMovieBackdrop();
      expect(path).toEqual(DEFAULT_MOVIE_BACKDROP_URL);
    });
  });

  describe("buildUrl function", () => {
    test("build url success with page and query params", () => {
      const url = buildUrl("movie/top_rates", { page: 1, query: "Blacklist" });

      expect(url).toEqual(
        `${Config.THE_MOVIE_DB_API_URL}/movie/top_rates?api_key=${
          Config.THE_MOVIE_DB_API_KEY
        }&page=1&query=Blacklist`
      );
    });
  });
});
