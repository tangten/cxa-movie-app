import { createSelector } from "reselect";

const getMovie = state => state.movie;

export const getTopMovies = createSelector(
  [getMovie],
  movie => {
    return movie.topMovieIds.map(movieId => movie.movies[movieId]);
  }
);

export const getPopularMovies = createSelector(
  [getMovie],
  movie => {
    return movie.popularMovieIds.map(movieId => movie.movies[movieId]);
  }
);

export const getSearchMovieResults = createSelector(
  [getMovie],
  movie => {
    return movie.searchMovieIds.map(movieId => movie.movies[movieId]);
  }
);

export const getUpcomingMovies = createSelector(
  [getMovie],
  movie => {
    return movie.upcomingMovieIds.map(movieId => movie.movies[movieId]);
  }
);
