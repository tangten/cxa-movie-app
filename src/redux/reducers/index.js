import { combineReducers } from "redux";
import movie from "./movie";
import pagination from "./pagination";

export default combineReducers({ movie, pagination });
