import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createStackNavigator } from "react-navigation-stack";
import { Icon } from "@shoutem/ui";

import HomeScreen from "~/screens/Home";
import SearchScreen from "~/screens/Search";
import MovieDetailsScreen from "~/screens/MovieDetails";
import { routes, tabIcons } from "~/constants";

const TabsNavigator = createBottomTabNavigator(
  {
    [routes.HOME]: {
      screen: HomeScreen
    },
    [routes.SEARCH]: {
      screen: SearchScreen
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: () => {
        const { routeName } = navigation.state;

        return <Icon name={tabIcons[routeName]} />;
      }
    })
  }
);

const AppNavigator = createStackNavigator(
  {
    [routes.APP_TABS]: {
      screen: TabsNavigator
    },
    [routes.MOVIE_DETAILS]: {
      screen: MovieDetailsScreen
    }
  },
  {
    headerMode: "none"
  }
);

export default AppNavigator;
