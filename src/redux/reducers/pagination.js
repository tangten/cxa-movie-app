import * as actionTypes from "~/redux/actionTypes";
import createReducer from "../createReducer";

export const initialState = {
  topMovies: {
    page: 0,
    totalPages: 0
  },
  popularMovies: {
    page: 0,
    totalPages: 0
  },
  searchMovies: {
    page: 0,
    totalPages: 0
  },
  latestMovies: {
    page: 0,
    totalPages: 0
  },
  upcomingMovies: {
    page: 0,
    totalPages: 0
  }
};

const handlersPagination = {
  [actionTypes.GET_TOP_MOVIES_SUCCESS]: (state, action) => {
    const { page, total_pages } = action.payload.data;
    return {
      ...state,
      topMovies: {
        page,
        totalPages: total_pages
      }
    };
  },
  [actionTypes.GET_POPULAR_MOVIES_SUCCESS]: (state, action) => {
    const { page, total_pages } = action.payload.data;
    return {
      ...state,
      popularMovies: {
        page,
        totalPages: total_pages
      }
    };
  },
  [actionTypes.SEARCH_MOVIE_SUCCESS]: (state, action) => {
    const { page, total_pages } = action.payload.data;

    return {
      ...state,
      searchMovies: {
        page,
        totalPages: total_pages
      }
    };
  },
  [actionTypes.CLEAR_SEARCH]: state => {
    return {
      ...state,
      searchMovies: {
        page: 0,
        totalPages: 0
      }
    };
  },
  [actionTypes.GET_UPCOMING_MOVIES_SUCCESS]: (state, action) => {
    const { page, total_pages } = action.payload.data;
    return {
      ...state,
      upcomingMovies: {
        page,
        totalPages: total_pages
      }
    };
  }
};

export default createReducer(initialState, handlersPagination);
