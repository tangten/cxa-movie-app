import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import movieReducer from "~/redux/reducers/movie";
import paginationReducer from "~/redux/reducers/pagination";

import * as actions from "~/redux/actions/movie";
import * as actionTypes from "~/redux/actionTypes";

const movieState = movieReducer(undefined, {});
const paginationState = paginationReducer(undefined, {});
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares)({
  movie: movieState,
  pagination: paginationState
});

describe("Movie actions", () => {
  it("creates GET_TOP_MOVIES_SUCCESS when getting top rated movies has been done", () => {});
  it("creates GET_TOP_MOVIES_FAILURE when getting top rated movies has some errors", () => {});

  it("creates GET_POPULAR_MOVIES_SUCCESS when getting popular rated movies has been done", () => {});
  it("creates GET_POPULAR_MOVIES_FAILURE when getting popular rated movies has some errors", () => {});

  it("creates SEARCH_MOVIES_SUCCESS when searching movies has been done", () => {});
  it("creates SEARCH_MOVIES_FAILURE when searching movies has some errors", () => {});

  it("creates CLEAR_SEARCH when user clear text in search box", () => {});

  it("creates GET_UPCOMING_MOVIES_SUCCESS when searching movies has been done", () => {});
  it("creates GET_UPCOMING_MOVIES_FAILURE when searching movies has some errors", () => {});
});
