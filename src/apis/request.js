const request = async (url, options = {}) => {
  const overridedOptions = {
    ...options,
    method: options.method || "GET"
  };

  const response = await fetch(url, overridedOptions);

  if (response.status === 401) {
    throw "Unauthorized";
  }

  if (response.status === 404) {
    throw "NotFound";
  }

  return await response.json();
};

export default request;
