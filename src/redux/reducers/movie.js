import * as actionTypes from "~/redux/actionTypes";
import { keyBy } from "lodash";
import createReducer from "../createReducer";

export const initialState = {
  topMovieIds: [],
  popularMovieIds: [],
  searchMovieIds: [],
  upcomingMovieIds: [],
  isLoadingTopMovies: false,
  isLoadingPopularMovies: false,
  isSearching: false,
  isLoadingUpcomingMovies: false,
  movies: {},
  crrSearchString: ""
};

const handlersMovie = {
  [actionTypes.GET_TOP_MOVIES_REQUEST]: state => {
    return {
      ...state,
      isLoadingTopMovies: true
    };
  },
  [actionTypes.GET_TOP_MOVIES_FAILURE]: state => {
    return {
      ...state,
      isLoadingTopMovies: false
    };
  },
  [actionTypes.GET_TOP_MOVIES_SUCCESS]: (state, action) => {
    const movies = keyBy(action.payload.data.results, "id");

    return {
      ...state,
      isLoadingTopMovies: false,
      topMovieIds: state.topMovieIds.concat(Object.keys(movies)),
      movies: {
        ...state.movies,
        ...movies
      }
    };
  },
  [actionTypes.GET_POPULAR_MOVIES_REQUEST]: state => {
    return {
      ...state,
      isLoadingPopularMovies: true
    };
  },
  [actionTypes.GET_POPULAR_MOVIES_FAILURE]: state => {
    return {
      ...state,
      isLoadingPopularMovies: false
    };
  },
  [actionTypes.GET_POPULAR_MOVIES_SUCCESS]: (state, action) => {
    const movies = keyBy(action.payload.data.results, "id");

    return {
      ...state,
      isLoadingPopularMovies: false,
      popularMovieIds: state.popularMovieIds.concat(Object.keys(movies)),
      movies: {
        ...state.movies,
        ...movies
      }
    };
  },
  [actionTypes.SEARCH_MOVIE_REQUEST]: state => {
    return {
      ...state,
      isSearching: true
    };
  },
  [actionTypes.SEARCH_MOVIE_FAILURE]: state => {
    return {
      ...state,
      isSearching: false
    };
  },
  [actionTypes.SEARCH_MOVIE_SUCCESS]: (state, action) => {
    const { data, searchString } = action.payload;
    const movies = keyBy(data.results, "id");
    const isNewSearch = searchString !== state.crrSearchString;
    const crrSearchMovieIds = isNewSearch ? [] : state.searchMovieIds;

    return {
      ...state,
      isSearching: false,
      searchMovieIds: crrSearchMovieIds.concat(Object.keys(movies)),
      crrSearchString: searchString,
      movies: {
        ...state.movies,
        ...movies
      }
    };
  },
  [actionTypes.CLEAR_SEARCH]: state => {
    return {
      ...state,
      isSearching: false,
      searchMovieIds: [],
      crrSearchString: ""
    };
  },
  [actionTypes.GET_UPCOMING_MOVIES_REQUEST]: state => {
    return {
      ...state,
      isLoadingUpcomingMovies: true
    };
  },
  [actionTypes.GET_UPCOMING_MOVIES_FAILURE]: state => {
    return {
      ...state,
      isLoadingUpcomingMovies: false
    };
  },
  [actionTypes.GET_UPCOMING_MOVIES_SUCCESS]: (state, action) => {
    const movies = keyBy(action.payload.data.results, "id");

    return {
      ...state,
      isLoadingUpcomingMovies: false,
      upcomingMovieIds: state.upcomingMovieIds.concat(Object.keys(movies)),
      movies: {
        ...state.movies,
        ...movies
      }
    };
  }
};

export default createReducer(initialState, handlersMovie);
