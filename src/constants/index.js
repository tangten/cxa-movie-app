export const DEFAULT_MOVIE_POSTER_URL =
  "https://media.comicbook.com/files/img/default-movie.png";

export const DEFAULT_MOVIE_BACKDROP_URL =
  "https://ak0.picdn.net/shutterstock/videos/14355040/thumb/10.jpg";

export const routes = {
  HOME: "HOME",
  SEARCH: "SEARCH",
  MOVIE_DETAILS: "MOVIE_DETAILS",
  APP_TABS: "APP_TABS"
};

export const tabIcons = {
  [routes.HOME]: "home",
  [routes.SEARCH]: "search"
};
