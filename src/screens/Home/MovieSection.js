import React from "react";
import { StyleSheet } from "react-native";
import { ListView, Image, Title, View, TouchableOpacity } from "@shoutem/ui";
import { getMoviePoster } from "~/utils";
import { routes } from "~/constants";

class MovieSection extends React.PureComponent {
  componentDidMount() {
    this.props.getMovies();
  }

  _goToMovieDetails = movieId => {
    this.props.navigation.navigate(routes.MOVIE_DETAILS, {
      movieId
    });
  };

  _renderRow = movie => {
    return (
      <View styleName="md-gutter-right">
        <TouchableOpacity
          onPress={() => {
            this._goToMovieDetails(movie.id);
          }}
        >
          <Image
            source={{ uri: getMoviePoster(movie.poster_path) }}
            styleName="rounded-corners"
            style={styles.posterImg}
          />
        </TouchableOpacity>
      </View>
    );
  };

  _onLoadMore = () => {
    const { getMovies, isLoading } = this.props;
    if (!isLoading) {
      getMovies();
    }
  };

  render() {
    const { isLoading, movies, title } = this.props;
    return (
      <View styleName="lg-gutter-top md-gutter-left">
        <Title styleName="md-gutter-bottom">{title}</Title>
        <ListView
          data={movies}
          renderRow={this._renderRow}
          horizontal
          loading={isLoading}
          onLoadMore={this._onLoadMore}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  posterImg: {
    width: 100,
    height: 150
  }
});

export default MovieSection;
