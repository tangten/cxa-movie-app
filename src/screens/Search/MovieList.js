import React from "react";
import { StyleSheet } from "react-native";
import {
  ListView,
  Image,
  View,
  Row,
  Title,
  Subtitle,
  Caption,
  TouchableOpacity
} from "@shoutem/ui";
import { getMoviePoster } from "~/utils";
import { routes } from "~/constants";

class MovieList extends React.PureComponent {
  _goToMovieDetails = movieId => {
    this.props.navigation.navigate(routes.MOVIE_DETAILS, { movieId });
  };

  _renderRow = movie => {
    return (
      <Row>
        <Image
          source={{ uri: getMoviePoster(movie.poster_path) }}
          styleName="rounded-corners"
          style={styles.posterImg}
        />
        <View styleName="vertical stretch space-between">
          <TouchableOpacity
            onPress={() => {
              this._goToMovieDetails(movie.id);
            }}
          >
            <Title>{movie.title}</Title>
          </TouchableOpacity>
          <Subtitle numberOfLines={3} ellipsizeMode="tail">
            {movie.overview}
          </Subtitle>
          <Caption>Published: {movie.release_date}</Caption>
        </View>
      </Row>
    );
  };

  _onLoadMore = () => {
    const { searchMovies, isSearching, crrSearchString } = this.props;
    if (!isSearching) {
      searchMovies(crrSearchString);
    }
  };

  render() {
    const { isSearching, movies } = this.props;
    return (
      <View>
        <ListView
          data={movies}
          renderRow={this._renderRow}
          loading={isSearching}
          onLoadMore={this._onLoadMore}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  posterImg: {
    width: 100,
    height: 150
  }
});

export default MovieList;
