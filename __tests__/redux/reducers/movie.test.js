import movieReducer, { initialState } from "~/redux/reducers/movie";

import * as actionTypes from "~/redux/actionTypes";

describe("Movie reducer", () => {
  it("should return the initial state", () => {
    expect(movieReducer(undefined, {})).toEqual(initialState);
  });

  it("should handle GET_TOP_MOVIES_REQUEST", () => {
    const action = {
      type: actionTypes.GET_TOP_MOVIES_REQUEST
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingTopMovies: true
    });
  });

  it("should handle GET_TOP_MOVIES_FAILURE", () => {
    const action = {
      type: actionTypes.GET_TOP_MOVIES_FAILURE
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingTopMovies: false
    });
  });

  it("should handle GET_TOP_MOVIES_SUCCESS", () => {
    const action = {
      type: actionTypes.GET_TOP_MOVIES_SUCCESS,
      payload: {
        data: {
          page: 1,
          total_pages: 10,
          results: [
            {
              id: "1"
            }
          ]
        }
      }
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingTopMovies: false,
      topMovieIds: ["1"],
      movies: {
        "1": { id: "1" }
      }
    });
  });

  it("should handle GET_POPULAR_MOVIES_REQUEST", () => {
    const action = {
      type: actionTypes.GET_POPULAR_MOVIES_REQUEST
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingPopularMovies: true
    });
  });

  it("should handle GET_POPULAR_MOVIES_FAILURE", () => {
    const action = {
      type: actionTypes.GET_POPULAR_MOVIES_FAILURE
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingPopularMovies: false
    });
  });

  it("should handle GET_POPULAR_MOVIES_SUCCESS", () => {
    const action = {
      type: actionTypes.GET_POPULAR_MOVIES_SUCCESS,
      payload: {
        data: {
          page: 1,
          total_pages: 10,
          results: [
            {
              id: "2"
            }
          ]
        }
      }
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingPopularMovies: false,
      popularMovieIds: ["2"],
      movies: {
        "2": { id: "2" }
      }
    });
  });

  it("should handle SEARCH_MOVIE_REQUEST", () => {
    const action = {
      type: actionTypes.SEARCH_MOVIE_REQUEST
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isSearching: true
    });
  });

  it("should handle SEARCH_MOVIE_FAILURE", () => {
    const action = {
      type: actionTypes.SEARCH_MOVIE_FAILURE
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isSearching: false
    });
  });

  it("should handle SEARCH_MOVIE_SUCCESS", () => {
    const action = {
      type: actionTypes.SEARCH_MOVIE_SUCCESS,
      payload: {
        data: {
          page: 1,
          total_pages: 10,
          results: [
            {
              id: "3",
              title: "The Blacklist"
            }
          ]
        },
        searchString: "Blacklist"
      }
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isSearching: false,
      searchMovieIds: ["3"],
      movies: {
        "3": { id: "3", title: "The Blacklist" }
      },
      crrSearchString: "Blacklist"
    });
  });

  it("should handle CLEAR_SEARCH", () => {
    const action = {
      type: actionTypes.CLEAR_SEARCH
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isSearching: false,
      searchMovieIds: [],
      crrSearchString: ""
    });
  });

  it("should handle GET_UPCOMING_MOVIES_REQUEST", () => {
    const action = {
      type: actionTypes.GET_UPCOMING_MOVIES_REQUEST
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingUpcomingMovies: true
    });
  });

  it("should handle GET_UPCOMING_MOVIES_FAILURE", () => {
    const action = {
      type: actionTypes.GET_UPCOMING_MOVIES_FAILURE
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingUpcomingMovies: false
    });
  });

  it("should handle GET_UPCOMING_MOVIES_SUCCESS", () => {
    const action = {
      type: actionTypes.GET_UPCOMING_MOVIES_SUCCESS,
      payload: {
        data: {
          page: 4,
          total_pages: 13,
          results: [
            {
              id: "3"
            }
          ]
        }
      }
    };

    expect(movieReducer(undefined, action)).toEqual({
      ...initialState,
      isLoadingUpcomingMovies: false,
      upcomingMovieIds: ["3"],
      movies: {
        "3": { id: "3" }
      }
    });
  });
});
