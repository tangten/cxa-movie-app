import React from "react";
import { StyleSheet } from "react-native";
import { View, TextInput, Icon, TouchableOpacity } from "@shoutem/ui";

class SearchBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchString: ""
    };
  }

  _onChangeText = value => {
    this.setState({ searchString: value });
  };

  _clearSearchString = () => {
    this.setState({ searchString: "" });
    this.props.clearSearch();
  };

  _onSubmitEditing = () => {
    const searchString = this.state.searchString.trim();

    if (
      this.props.crrSearchString.trim() !== searchString &&
      searchString !== ""
    ) {
      this.props.search(searchString);
    }
  };

  render() {
    const {} = this.props;
    const { searchString } = this.state;

    return (
      <View>
        <TextInput
          placeholder="Search"
          onChangeText={this._onChangeText}
          onSubmitEditing={this._onSubmitEditing}
          value={searchString}
        />
        {!!searchString.trim() && (
          <TouchableOpacity
            onPress={this._clearSearchString}
            style={styles.closeIconWrapper}
          >
            <Icon name="close" />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  closeIconWrapper: {
    position: "absolute",
    top: 15,
    right: 15
  }
});

export default SearchBox;
