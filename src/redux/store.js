import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
import { compose, createStore, applyMiddleware } from "redux";

import rootReducer from "./reducers";

const loggerMiddleware = createLogger();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunkMiddleware, loggerMiddleware))
);

export default store;
