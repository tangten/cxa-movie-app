import React from "react";
import { connect } from "react-redux";
import { ScrollView } from "react-native";
import {
  Screen,
  Title,
  Button,
  NavigationBar,
  Icon,
  Image,
  View,
  Caption,
  Subtitle
} from "@shoutem/ui";
import { getMovieBackdrop } from "~/utils";

class MovieDetails extends React.Component {
  _goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { movie } = this.props;
    return (
      <Screen>
        <NavigationBar
          leftComponent={
            <Button onPress={this._goBack}>
              <Icon name="back" />
            </Button>
          }
          centerComponent={<Title>CXA MOVIES</Title>}
          styleName="inline"
          hasHistory
        />
        <ScrollView>
          <Image
            styleName="large-wide"
            source={{
              uri: getMovieBackdrop(movie.backdrop_path)
            }}
          />
          <View styleName="md-gutter">
            <Title>{movie.title}</Title>
            <Subtitle>{movie.overview}</Subtitle>
          </View>
        </ScrollView>
      </Screen>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    movie: state.movie.movies[ownProps.navigation.state.params.movieId]
  };
};
export default connect(mapStateToProps)(MovieDetails);
