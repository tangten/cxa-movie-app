# CXA Movie

CXA Movie app provides top rated or popular movies from TheMovieDB.

## Supported Platforms

- [x] iOS
- [x] Android
- [ ] Windows

## Technicals

- React Native 0.60.5
- React Navigation - Manage routers
- Redux - Manage state
- Redux Thunk - Support redux async actions
- Shoutem UI

## How to run

```
git clone https://gitlab.com/tangten/cxa-movie-app.git
cd cxa-movie-app
yarn i
cd ios
pod install
```

Prepare environment variables:

```
Add `.env` file in root project then copy `env.example` content file into `.env`
Fill all env vars value
```

And

```
yarn run-ios
```

Or

```
yarn run-android
```

## Implementation Status

- [x] Get top rated movies
- [x] Paginate for getting rated movies
- [x] Get popular movies
- [x] Paginate for getting popular movies
- [x] Search movies
- [x] Paginate for searching movies
- [x] Add movie details screen
- [x] Add more section for home screen (upcoming)
- [ ] Add more unitest

## App Preview:

### Screen Showcase

<div align="center">

![CXA Movie App Home Screen](https://i.imgur.com/TCVOz8L.png)
![CXA Movie App Search Screen](https://i.imgur.com/qgGDti9.png)
![CXA Movie App Movie Details Screen](https://i.imgur.com/4tTkXG5.png)

</div>

### Video Preview

<div align="center">

![CXA Movie App Video Preview](https://i.imgur.com/7TBgWmr.mp4)

</div>
