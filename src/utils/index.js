import Config from "react-native-config";
import qs from "querystringify";
import {
  DEFAULT_MOVIE_POSTER_URL,
  DEFAULT_MOVIE_BACKDROP_URL
} from "~/constants";

export const getMoviePoster = path => {
  if (!path) return DEFAULT_MOVIE_POSTER_URL;
  return `${Config.THE_MOVIE_DB_POSTER_PATH}/${path}`;
};

export const getMovieBackdrop = path => {
  if (!path) return DEFAULT_MOVIE_BACKDROP_URL;
  return `${Config.THE_MOVIE_DB_POSTER_PATH}/${path}`;
};

export const buildUrl = (path, params) => {
  const queryString = qs.stringify(params);

  return `${Config.THE_MOVIE_DB_API_URL}/${path}?api_key=${
    Config.THE_MOVIE_DB_API_KEY
  }&${queryString}`;
};
