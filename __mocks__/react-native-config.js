export default {
  THE_MOVIE_DB_API_KEY: "THE_MOVIE_DB_API_KEY",
  THE_MOVIE_DB_API_URL: "https://themoviedb.api/3",
  THE_MOVIE_DB_POSTER_PATH: "https://themoviedb.image"
};
