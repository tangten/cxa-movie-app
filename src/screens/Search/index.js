import React from "react";
import { connect } from "react-redux";
import { NavigationBar, Title, Screen, Divider } from "@shoutem/ui";
import { searchMovies, clearSearch } from "~/redux/actions/movie";
import { getSearchMovieResults as searchMovieResults } from "~/redux/selectors";

import SearchBox from "./SearchBox";
import MovieList from "./MovieList";

class SearchScreen extends React.Component {
  render() {
    const {
      searchMovies,
      crrSearchString,
      searchMovieResults,
      isSearching,
      clearSearch,
      navigation
    } = this.props;

    return (
      <Screen>
        <NavigationBar
          centerComponent={<Title>CXA MOVIES</Title>}
          styleName="inline"
        />
        <SearchBox
          crrSearchString={crrSearchString}
          search={searchMovies}
          clearSearch={clearSearch}
        />
        <Divider styleName="line" />
        <MovieList
          isSearching={isSearching}
          crrSearchString={crrSearchString}
          movies={searchMovieResults}
          searchMovies={searchMovies}
          navigation={navigation}
        />
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  return {
    isSearching: state.movie.isSearching,
    crrSearchString: state.movie.crrSearchString,
    searchMovieResults: searchMovieResults(state)
  };
};

export default connect(
  mapStateToProps,
  { searchMovies, clearSearch }
)(SearchScreen);
