import * as actionTypes from "~/redux/actionTypes";
import * as movieApi from "~/apis/movie";

const getTopMoviesRequest = () => ({
  type: actionTypes.GET_TOP_MOVIES_REQUEST
});

const getTopMoviesSuccess = data => ({
  type: actionTypes.GET_TOP_MOVIES_SUCCESS,
  payload: { data }
});

const getTopMoviesFailure = err => ({
  type: actionTypes.GET_TOP_MOVIES_FAILURE,
  payload: { err }
});

export const getTopMovies = () => (dispatch, getState) => {
  const { page } = getState().pagination.topMovies;

  dispatch(getTopMoviesRequest());

  return movieApi
    .getTopMovies(page + 1)
    .then(data => {
      if (page < data.total_pages) {
        dispatch(getTopMoviesSuccess(data));
      }
    })
    .catch(err => dispatch(getTopMoviesFailure(err)));
};

const getPopularMoviesRequest = () => ({
  type: actionTypes.GET_POPULAR_MOVIES_REQUEST
});

const getPopularMoviesSuccess = data => ({
  type: actionTypes.GET_POPULAR_MOVIES_SUCCESS,
  payload: { data }
});

const getPopularMoviesFailure = err => ({
  type: actionTypes.GET_POPULAR_MOVIES_FAILURE,
  payload: { err }
});

export const getPopularMovies = () => (dispatch, getState) => {
  const { page } = getState().pagination.popularMovies;

  dispatch(getPopularMoviesRequest());

  return movieApi
    .getPopularMovies(page + 1)
    .then(data => {
      if (page < data.total_pages) {
        dispatch(getPopularMoviesSuccess(data));
      }
    })
    .catch(err => dispatch(getPopularMoviesFailure(err)));
};

const searchMoviesRequest = () => ({
  type: actionTypes.SEARCH_MOVIE_REQUEST
});

const searchMoviesSuccess = (data, searchString) => ({
  type: actionTypes.SEARCH_MOVIE_SUCCESS,
  payload: { data, searchString }
});

const searchMoviesFailure = err => ({
  type: actionTypes.SEARCH_MOVIE_FAILURE,
  payload: { err }
});

export const searchMovies = searchString => (dispatch, getState) => {
  const { pagination, movie } = getState();
  const { page } = pagination.searchMovies;
  const { crrSearchString } = movie;

  const isNewSearch = searchString !== crrSearchString;

  dispatch(searchMoviesRequest());

  return movieApi
    .searchMovies(isNewSearch ? 1 : page + 1, searchString)
    .then(data => {
      if (page < data.total_pages || isNewSearch) {
        dispatch(searchMoviesSuccess(data, searchString));
      }
    })
    .catch(err => dispatch(searchMoviesFailure(err)));
};

export const clearSearch = () => {
  return {
    type: actionTypes.CLEAR_SEARCH
  };
};

const getUpcomingMoviesRequest = () => ({
  type: actionTypes.GET_UPCOMING_MOVIES_REQUEST
});

const getUpcomingMoviesSuccess = data => ({
  type: actionTypes.GET_UPCOMING_MOVIES_SUCCESS,
  payload: { data }
});

const getUpcomingMoviesFailure = err => ({
  type: actionTypes.GET_UPCOMING_MOVIES_FAILURE,
  payload: { err }
});

export const getUpcomingMovies = () => (dispatch, getState) => {
  const { page } = getState().pagination.upcomingMovies;

  dispatch(getUpcomingMoviesRequest());

  return movieApi
    .getUpcomingMovies(page + 1)
    .then(data => {
      if (page < data.total_pages) {
        dispatch(getUpcomingMoviesSuccess(data));
      }
    })
    .catch(err => dispatch(getUpcomingMoviesFailure(err)));
};
