import paginationReducer, { initialState } from "~/redux/reducers/pagination";

import * as actionTypes from "~/redux/actionTypes";

describe("Pagination reducer", () => {
  it("should return the initial state", () => {
    expect(paginationReducer(undefined, {})).toEqual(initialState);
  });

  it("should handle GET_TOP_MOVIES_SUCCESS", () => {
    const action = {
      type: actionTypes.GET_TOP_MOVIES_SUCCESS,
      payload: {
        data: {
          page: 1,
          total_pages: 10,
          results: [
            {
              id: "1"
            }
          ]
        }
      }
    };

    expect(paginationReducer(undefined, action)).toEqual({
      ...initialState,
      topMovies: {
        page: 1,
        totalPages: 10
      }
    });
  });

  it("should handle GET_POPULAR_MOVIES_SUCCESS", () => {
    const action = {
      type: actionTypes.GET_POPULAR_MOVIES_SUCCESS,
      payload: {
        data: {
          page: 2,
          total_pages: 11,
          results: [
            {
              id: "2"
            }
          ]
        }
      }
    };

    expect(paginationReducer(undefined, action)).toEqual({
      ...initialState,
      popularMovies: {
        page: 2,
        totalPages: 11
      }
    });
  });

  it("should handle SEARCH_MOVIE_SUCCESS", () => {
    const action = {
      type: actionTypes.SEARCH_MOVIE_SUCCESS,
      payload: {
        data: {
          page: 3,
          total_pages: 12,
          results: [
            {
              id: "3",
              title: "The Blacklist"
            }
          ]
        },
        searchString: "Blacklist"
      }
    };

    expect(paginationReducer(undefined, action)).toEqual({
      ...initialState,
      searchMovies: {
        page: 3,
        totalPages: 12
      }
    });
  });

  it("should handle CLEAR_SEARCH", () => {
    const action = {
      type: actionTypes.CLEAR_SEARCH
    };

    expect(paginationReducer(undefined, action)).toEqual({
      ...initialState,
      searchMovies: {
        page: 0,
        totalPages: 0
      }
    });
  });

  it("should handle GET_UPCOMING_MOVIES_SUCCESS", () => {
    const action = {
      type: actionTypes.GET_UPCOMING_MOVIES_SUCCESS,
      payload: {
        data: {
          page: 2,
          total_pages: 11,
          results: [
            {
              id: "2"
            }
          ]
        }
      }
    };

    expect(paginationReducer(undefined, action)).toEqual({
      ...initialState,
      upcomingMovies: {
        page: 2,
        totalPages: 11
      }
    });
  });
});
