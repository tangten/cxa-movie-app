import request from "./request";
import { buildUrl } from "~/utils";

export const getTopMovies = page => {
  return request(buildUrl("movie/top_rated", { page }));
};

export const getPopularMovies = page => {
  return request(buildUrl("movie/popular", { page }));
};

export const getUpcomingMovies = page => {
  return request(buildUrl("movie/upcoming", { page }));
};

export const searchMovies = (page, query) => {
  return request(buildUrl("search/movie", { page, query }));
};
