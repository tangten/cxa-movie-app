import React from "react";
import { createAppContainer } from "react-navigation";
import { Provider } from "react-redux";
import AppNavigator from "~/navigation/AppNavigator";
import store from "~/redux/store";

const App = createAppContainer(AppNavigator);

const AppWrapper = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default AppWrapper;
